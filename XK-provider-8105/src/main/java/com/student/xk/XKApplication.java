package com.student.xk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class XKApplication {
    public static void main(String[] args) {
        SpringApplication.run(XKApplication.class,args);
    }
}
