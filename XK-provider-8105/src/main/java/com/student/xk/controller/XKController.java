package com.student.xk.controller;

import com.student.xclass.entity.XClass;
import com.student.xk.service.XKService;
import com.student.xk.utils.DataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/xk")
public class XKController {
    @Autowired
    private XKService xkService;
    @GetMapping("/add")
    public DataResult addXK(@RequestParam("xh")Long xh,@RequestParam("cid")Long cid){
        int result = xkService.addXK(xh, cid);
        if (result != 0) {
            return new DataResult(200, "成功添加课程！", null);
        } else {
            return new DataResult(400, "操作失败，请重试！", null);
        }
    }
    @GetMapping("/find")
    public DataResult find(@RequestParam("xh")Long xh){
        List<XClass> all = xkService.find(xh);
        return new DataResult(200, "选修课程", all);
    }
}
