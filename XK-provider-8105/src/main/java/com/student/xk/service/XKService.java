package com.student.xk.service;

import com.student.xclass.entity.XClass;

import java.util.List;

public interface XKService {
    //选课
    public int addXK(Long xh,Long cid);
    //查询
    public List<XClass> find(Long xh);
}
