package com.student.xk.service.impl;

import com.student.xclass.entity.XClass;
import com.student.xk.dao.XKDao;
import com.student.xk.service.XKService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class XKServiceImpl implements XKService {
    @Resource
    private XKDao xkDao;
    @Override
    public int addXK(Long xh, Long cid) {
        return xkDao.addXK(xh,cid);
    }

    @Override
    public List<XClass> find(Long xh) {
        return xkDao.find(xh);
    }
}
