package com.student.xk.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataResult<T> {

    private Integer code;
    private String message;
    private T data;

    public DataResult(Integer code, String message) {
        this(code, message, null);
    }

}
