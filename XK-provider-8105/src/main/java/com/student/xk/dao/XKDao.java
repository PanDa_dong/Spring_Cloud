package com.student.xk.dao;

import com.student.xclass.entity.XClass;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface XKDao {
    public int addXK(@Param("xh") Long xh, @Param("cid") Long cid);
    public List<XClass> find(@Param("xh") Long xh);
}
