package com.student.test.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ConfigPropertiesRepository extends JpaRepository<ConfigProperties, Long> {
}
