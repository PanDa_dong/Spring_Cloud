package com.student.test.controller;

import com.student.test.entity.ConfigProperties;
import com.student.test.entity.ConfigPropertiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test")
public class ConfigPropertiesController {
    @Autowired
    ConfigPropertiesRepository configPropertiesRepository;
    @Value("${server.port}")
    private String port;

    /**
     * 查
     *
     * @return
     */
    @GetMapping(value = "/list")
    public List<ConfigProperties> getConfigPropertiesList() {
        return configPropertiesRepository.findAll();
    }

    @GetMapping("/p")
    public String getPort() {
        System.out.println("=========port=======");
        return "port:"+port;
    }

    /*    *//**
     * 增
     * @param username
     * @param password
     * @return
     *//*
    @PostMapping(value = "/addConfigProperties")
    public ConfigProperties addConfigProperties(@RequestParam("username") String username,
                              @RequestParam("password") String password){
        ConfigProperties configpropertites = new ConfigProperties();
        configpropertites.setConfigPropertiesname(username);
        configpropertites.setPassword(password);
        return configPropertiesRepository.save(configpropertites);
    }

    *//**
     * 改
     * @param id
     * @param username
     * @param password
     * @return
     *//*
    @PutMapping(value = "updConfigProperties/{id}")
    public ConfigProperties updConfigProperties(@PathVariable("id") Integer id,
                              @RequestParam("username") String username,
                              @RequestParam("password") String password){
        ConfigProperties configpropertites = new ConfigProperties();
        configpropertites.setId(id);
        configpropertites.setConfigPropertiesname(username);
        configpropertites.setPassword(password);
        return configPropertiesRepository.save(configpropertites);

    }

    *//**
     * 删
     * @param id
     *//*
    @DeleteMapping(value = "delConfigProperties/{id}")
    public void delConfigProperties(@PathVariable("id") Integer id){
        ConfigProperties configpropertites = new ConfigProperties();
        configpropertites.setId(id);
        configPropertiesRepository.delete(configpropertites);
    }*/
}
