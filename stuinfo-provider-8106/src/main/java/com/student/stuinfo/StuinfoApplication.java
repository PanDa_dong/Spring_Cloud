package com.student.stuinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class StuinfoApplication {
    public static void main(String[] args) {
        SpringApplication.run(StuinfoApplication.class,args);
    }
}
