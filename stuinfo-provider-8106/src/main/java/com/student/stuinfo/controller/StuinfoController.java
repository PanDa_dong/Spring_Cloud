package com.student.stuinfo.controller;

import com.student.stuinfo.model.Stuinfo;
import com.student.stuinfo.service.StuinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/stuinfo")
public class StuinfoController {
    @Autowired
    private StuinfoService stuinfoService;

    @GetMapping("/deleteStuinfo")
    public String deleteStuinfo(@RequestParam("xh")String xh){
        System.out.println(xh);
        stuinfoService.deleteByXh(xh);
        return "ok";

    }
    @GetMapping("/findByXh")
    public Stuinfo findByXh(@RequestParam("xh")String xh){
        return stuinfoService.findByXh(xh);
    }
    @GetMapping("/findAllStuinfo")
    public List<Stuinfo> findAll(){
        return stuinfoService.findAll();
    }
    @GetMapping("/addStuinfo")
    public String addStuinfo(@RequestParam("xh")String xh,
                                                @RequestParam("sname")String sname,
                                                @RequestParam("sex")Integer sex,
                                                @RequestParam("age")Integer age,
                                                @RequestParam("gid")Integer gid,
                                                @RequestParam("sstatus")Integer sstatus){
        Stuinfo stuinfo=new Stuinfo();
        stuinfo.setAge(age);
        stuinfo.setGid(gid);
        stuinfo.setSex(sex);
        stuinfo.setSname(sname);
        stuinfo.setXh(xh);
        stuinfo.setSstatus(sstatus);
        stuinfoService.addByStuinfo(stuinfo);
        return "ok";
    }
    @GetMapping("/updateStuinfo")
    public String upadateStuinfo(@RequestParam(value = "xh")String xh,
                                             @RequestParam(value ="sname",required = false)String sname,
                                             @RequestParam(value ="sex",required = false)Integer sex,
                                             @RequestParam(value ="age",required = false)Integer age,
                                             @RequestParam(value ="gid",required = false)Integer gid,
                                             @RequestParam(value ="sstatus",required = false)Integer sstatus){
        Stuinfo stuinfo=new Stuinfo();
        stuinfo.setAge(age);
        stuinfo.setGid(gid);
        stuinfo.setSex(sex);
        stuinfo.setSname(sname);
        stuinfo.setXh(xh);
        stuinfo.setSstatus(sstatus);
        stuinfoService.updateByXh(stuinfo);
        return "ok";
    }



}
