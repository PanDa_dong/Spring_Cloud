package com.student.stuinfo.dao;

import com.student.stuinfo. model.Stuinfo;
import org.apache.ibatis.annotations.*;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;
@Mapper
public interface StuinfoDao {
    @Delete({"delete from stuinfo where xh = #{xh}"})
    int deleteByXh(String xh);

    @Select({"select * from stuinfo where xh=#{xh}"})
    Stuinfo findByXh(String xh);

    @Select({"select * from stuinfo"})
    List<Stuinfo > findAll();

    @Insert({"insert into stuinfo (xh,sname, sex, age,gid, sstatus)values (#{xh},#{sname}, #{sex}, #{age},#{gid}, #{sstatus})"})
    int insert(Stuinfo record);

    @Insert("<script>\n"+
            "    insert into stuinfo\n"+
            "    <trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\">\n"+
            "      <if test=\"sname != null\">\n"+
            "        sname,\n"+
            "      </if>\n"+
            "      <if test=\"sex != null\">\n"+
            "        sex,\n"+
            "      </if>\n"+
            "      <if test=\"age != null\">\n"+
            "        age,\n"+
            "      </if>\n"+
            "      <if test=\"gid != null\">\n"+
            "        gid,\n"+
            "      </if>\n"+
            "      <if test=\"sstatus != null\">\n"+
            "        sstatus,\n"+
            "      </if>\n"+
            "    </trim>\n"+
            "    <trim prefix=\"values (\" suffix=\")\" suffixOverrides=\",\">\n"+
            "      <if test=\"sname != null\">\n"+
            "        #{sname,jdbcType=VARCHAR},\n"+
            "      </if>\n"+
            "      <if test=\"sex != null\">\n"+
            "        #{sex,jdbcType=INTEGER},\n"+
            "      </if>\n"+
            "      <if test=\"age != null\">\n"+
            "        #{age,jdbcType=INTEGER},\n"+
            "      </if>\n"+
            "      <if test=\"gid != null\">\n"+
            "        #{gid,jdbcType=INTEGER},\n"+
            "      </if>\n"+
            "      <if test=\"sstatus != null\">\n"+
            "        #{sstatus,jdbcType=INTEGER},\n"+
            "      </if>\n"+
            "    </trim>\n"+
            "    </script>")
    int insertSelective(Stuinfo record);

    @Results(id = "BaseResultMap",
            value = {
//<id column="xh" jdbcType="VARCHAR" property="xh"/>
                    @Result(column = "xh", property = "xh"),
                    @Result(column = "sname", property = "sname"),
                    @Result(column = "sex", property = "sex"),
                    @Result(column = "age", property = "age"),
                    @Result(column = "gid", property = "gid"),
                    @Result(column = "sstatus", property = "sstatus"),
            })


    @Update("<script>\n"+
            "    update stuinfo\n"+
            "    <set>\n"+
            "      <if test=\"sname != null\">\n"+
            "        sname = #{sname,jdbcType=VARCHAR},\n"+
            "      </if>\n"+
            "      <if test=\"sex != null\">\n"+
            "        sex = #{sex,jdbcType=INTEGER},\n"+
            "      </if>\n"+
            "      <if test=\"age != null\">\n"+
            "        age = #{age,jdbcType=INTEGER},\n"+
            "      </if>\n"+
            "      <if test=\"gid != null\">\n"+
            "        gid = #{gid,jdbcType=INTEGER},\n"+
            "      </if>\n"+
            "      <if test=\"sstatus != null\">\n"+
            "        sstatus = #{sstatus,jdbcType=INTEGER},\n"+
            "      </if>\n"+
            "    </set>\n"+
            "    where xh = #{xh,jdbcType=VARCHAR}\n"+
            "    </script>")
    int updateByPrimaryKeySelective(Stuinfo record);

    @Update("<script>\n"+
            "    update stuinfo\n"+
            "    set sname = #{sname,jdbcType=VARCHAR},\n"+
            "      sex = #{sex,jdbcType=INTEGER},\n"+
            "      age = #{age,jdbcType=INTEGER},\n"+
            "      gid = #{gid,jdbcType=INTEGER},\n"+
            "      sstatus = #{sstatus,jdbcType=INTEGER}\n"+
            "    where xh = #{xh,jdbcType=VARCHAR}\n"+
            "    </script>")
    int updateByPrimaryKey(Stuinfo record);





}
