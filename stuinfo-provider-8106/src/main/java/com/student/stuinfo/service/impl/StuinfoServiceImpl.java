package com.student.stuinfo.service.impl;

import com.student.stuinfo.dao.StuinfoDao;
import com.student.stuinfo.model.Stuinfo;
import com.student.stuinfo.service.StuinfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class StuinfoServiceImpl implements StuinfoService {
    @Resource
    StuinfoDao stuinfoDao;
    @Override
    public int deleteByXh(String xh) {
        stuinfoDao.deleteByXh(xh);
        return 0;
    }

    @Override
    public int addByStuinfo(Stuinfo stuinfo) {
        stuinfoDao.insert(stuinfo);
        return 0;
    }

    @Override
    public Stuinfo findByXh(String xh) {
        return stuinfoDao.findByXh(xh);
    }

    @Override
    public List<Stuinfo> findAll() {
        return stuinfoDao.findAll();
    }

    @Override
    public int updateByXh(Stuinfo stuinfo) {
        stuinfoDao.updateByPrimaryKey(stuinfo);
        return 0;
    }
}
