package com.student.stuinfo.service;

import com.student.stuinfo.model.Stuinfo;

import java.util.List;

public interface StuinfoService {
    int deleteByXh(String xh);
    int addByStuinfo(Stuinfo stuinfo);
    Stuinfo findByXh(String xh);
    List<Stuinfo> findAll();
    int updateByXh(Stuinfo stuinfo);
}
