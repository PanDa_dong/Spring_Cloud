package com.student.stuinfo.model;

import java.io.Serializable;

/**
 * stuinfo
 * @author
 */
public class Stuinfo implements Serializable {
    private String xh;

    private String sname;

    private Integer sex;

    private Integer age;

    private Integer gid;

    /**
     * 1-在校 0-离校
     */
    private Integer sstatus;

    private static final long serialVersionUID = 1L;

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Integer getSstatus() {
        return sstatus;
    }

    public void setSstatus(Integer sstatus) {
        this.sstatus = sstatus;
    }
}
