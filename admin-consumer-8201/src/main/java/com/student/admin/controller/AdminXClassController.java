package com.student.admin.controller;


import com.student.admin.service.AdminXClassServiceClient;
import com.student.admin.util.RedisUtil;
import com.student.xclassapi.api.XClassServiceAPI;
import com.student.xclassapi.utils.DataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/class")
public class AdminXClassController {
    @Autowired
    private AdminXClassServiceClient xClassService;


    @Autowired
    private RedisUtil redisUtil;


    @GetMapping("/findAll")
    public DataResult findAllClass(@RequestParam("uuid") String uuid) {
        if (verification(uuid))
        return xClassService.findAllClass();
        return new DataResult(001,"非法请求！",null);
    }

    @GetMapping("/find")
    public DataResult findClass(@RequestParam("uuid") String uuid,@RequestParam("cid") Long cid, @RequestParam("cname") String cname) {
        if (verification(uuid))
        return xClassService.findClass(cid, cname);
        return new DataResult(001,"非法请求！",null);
    }

    @PostMapping("/add")
    public DataResult addClass(@RequestParam("uuid") String uuid,@RequestParam("cname") String cname, @RequestParam("info") String info, @RequestParam("cstatus") int cstatus) {
        if (verification(uuid))
        return xClassService.addClass(cname, info, cstatus);
        return new DataResult(001,"非法请求！",null);
    }

    @GetMapping("/del")
    public DataResult delClass(@RequestParam("uuid") String uuid,@RequestParam("cid") Long cid) {
        if (verification(uuid))
        return xClassService.delClass(cid);
        return new DataResult(001,"非法请求！",null);
    }

    @PostMapping("/update")
    public DataResult updateClass(@RequestParam("uuid") String uuid,@RequestParam("cid") Long cid, @RequestParam("cname") String cname, @RequestParam("info") String info, @RequestParam("cstatus") int cstatus) {
        if (verification(uuid))
        return xClassService.updateClass(cid, cname, info, cstatus);
        return new DataResult(001,"非法请求！",null);
    }

    public boolean verification(String uuid){
        Object role = redisUtil.get(uuid);
        if (role==null)return false;
        if (role.toString().equals("0")){
            return true;
        }else{
            return false;
        }
    }
}
