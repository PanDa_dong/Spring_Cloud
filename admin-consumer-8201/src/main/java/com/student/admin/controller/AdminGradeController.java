package com.student.admin.controller;

import com.student.admin.service.AdminAccountServiceClient;
import com.student.admin.service.AdminGradeServiceFeign;
import com.student.admin.util.RedisUtil;
import com.student.xclassapi.utils.DataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/admin/grade")
public class AdminGradeController {
    @Resource
    private AdminGradeServiceFeign gradeServiceFeign;
    @Autowired
    private RedisUtil redisUtil;
    @GetMapping("/findAll")
    public DataResult findAllClass(@RequestParam("uuid") String uuid) {
        if (verification(uuid))
            return gradeServiceFeign.findAll();
        return new DataResult(001,"非法请求！",null);
    }
    @PostMapping("/add")
    public DataResult addGrade(@RequestParam("uuid")String uuid,@RequestParam("gid")Long gid, @RequestParam("gname")String gname, @RequestParam("gstatus") Integer gstatus){
        if (verification(uuid))
            return gradeServiceFeign.addGrade(gid, gname, gstatus);
        return new DataResult(001,"非法请求！",null);
    }
    @GetMapping("/del")
    public DataResult delGrade(@RequestParam("uuid")String uuid,@RequestParam("gid")Long gid){
        if (verification(uuid))
            return gradeServiceFeign.delGrade(gid);
        return new DataResult(001,"非法请求！",null);
    }
    @PostMapping("/update")
    public DataResult update(@RequestParam("uuid")String uuid,@RequestParam("gid")Long gid, @RequestParam("gname")String gname, @RequestParam("gstatus") Integer gstatus){
        if (verification(uuid))
            return gradeServiceFeign.update(gid, gname, gstatus);
        return new DataResult(001,"非法请求！",null);
    }

    public boolean verification(String uuid){
        Object role = redisUtil.get(uuid);
        if (role==null)return false;
        if (role.toString().equals("0")){
            return true;
        }else{
            return false;
        }
    }
}
