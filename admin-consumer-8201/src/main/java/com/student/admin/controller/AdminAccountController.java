package com.student.admin.controller;

import com.student.accountapi.utils.DataResult;
import com.student.admin.service.AdminAccountServiceClient;
import com.student.admin.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/account")
public class AdminAccountController {
    @Autowired
    private AdminAccountServiceClient accountService;
    @Autowired
    private RedisUtil redisUtil;
//    public DataResult


    @PostMapping("/add")
    public DataResult addUser(@RequestParam("uuid")String uuid,@RequestParam("uid")Long uid, @RequestParam("uname")String uname, @RequestParam("password") String password){
        if (verification(uuid))
        return accountService.addUser(uid,uname,password);
        return new DataResult(002,"非法请求！",null);
    }
    @GetMapping("/del")
    public DataResult delUser(@RequestParam("uuid")String uuid,@RequestParam("uid")Long uid){
        if (verification(uuid))
        return accountService.delUser(uid);
        return new DataResult(002,"非法请求！",null);
    }
    @GetMapping("/find")
    public DataResult find(@RequestParam("uuid")String uuid,@RequestParam("uid")Long uid, @RequestParam("uname")String uname){
        if (verification(uuid))
        return accountService.find(uid,uname);
        return new DataResult(002,"非法请求！",null);
    }
    @GetMapping("/findAll")
    public DataResult findAll(@RequestParam("uuid")String uuid){
        if (verification(uuid))
        return accountService.findAll();
        return new DataResult(002,"非法请求！",null);
    }
    @PostMapping("/update")
    public DataResult update(@RequestParam("uuid")String uuid,@RequestParam("uid")Long uid, @RequestParam("uname")String uname, @RequestParam("password") String password, @RequestParam("role")Integer role, @RequestParam("ustatus")Integer ustatus){
        if (verification(uuid))
        return accountService.update(uid,uname,password,role,ustatus);
        return new DataResult(002,"非法请求！",null);

    }

    /*身份验证*/
    public boolean verification(String uuid){
        Object role = redisUtil.get(uuid);
        if (role==null)return false;
        if (role.toString().equals("0")){
            return true;
        }else{
            return false;
        }
    }
}
