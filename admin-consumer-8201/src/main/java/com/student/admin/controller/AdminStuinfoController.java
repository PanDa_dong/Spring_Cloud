package com.student.admin.controller;

import com.student.admin.service.AdminStuinfoServiceFeign;
import com.student.stuinfo.model.Stuinfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/stuinfo")
public class AdminStuinfoController {
    @Resource
    private AdminStuinfoServiceFeign stuinfoServiceFeign;


    @GetMapping("/del")
        public String deleteStuinfo(@RequestParam("xh")String xh){
            return stuinfoServiceFeign.deleteStuinfo(xh);
        }
    @GetMapping("/findByXh")
    public Stuinfo findByXh(@RequestParam("xh")String xh){
            return stuinfoServiceFeign.findByXh(xh);
        }
    @GetMapping("/findAll")
    public List<Stuinfo> findAll(){
            return stuinfoServiceFeign.findAll();
        }
    @GetMapping("/add")
    public String addStuinfo(@RequestParam("xh")String xh,@RequestParam(value ="sname",required = false)String sname,
                                 @RequestParam(value ="sex",required = false)Integer sex,
                                 @RequestParam(value ="age",required = false)Integer age,
                                 @RequestParam(value ="gid",required = false)Integer gid,
                                 @RequestParam(value ="sstatus",required = false)Integer sstatus){
            return stuinfoServiceFeign.addStuinfo(xh, sname, sex, age, gid, sstatus);
        }
    @GetMapping("/update")
    public String upadateStuinfo(@RequestParam(value = "xh")String xh,
                                     @RequestParam(value ="sname",required = false)String sname,
                                     @RequestParam(value ="sex",required = false)Integer sex,
                                     @RequestParam(value ="age",required = false)Integer age,
                                     @RequestParam(value ="gid",required = false)Integer gid,
                                     @RequestParam(value ="sstatus",required = false)Integer sstatus){
            return stuinfoServiceFeign.upadateStuinfo(xh, sname, sex, age, gid, sstatus);
        }
}
