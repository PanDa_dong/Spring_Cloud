package com.student.admin.service;

import com.student.admin.service.fallback.AdminGradeFallback;
import com.student.xclassapi.utils.DataResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.xml.crypto.Data;

@FeignClient(value = "grade",fallback = AdminGradeFallback.class)
public interface AdminGradeServiceFeign {
    @PostMapping("/grade/add")
    public DataResult addGrade(@RequestParam("gid")Long gid,@RequestParam("gname")String gname,@RequestParam("gstatus")Integer gstatus);
    @GetMapping("/grade/del")
    public DataResult delGrade(@RequestParam("gid")Long gid);
    @GetMapping("/grade/findAll")
    public DataResult findAll();
    @PostMapping("/grade/update")
    public DataResult update(@RequestParam("gid")Long gid, @RequestParam("gname")String gname, @RequestParam("gstatus") Integer gstatus);
}
