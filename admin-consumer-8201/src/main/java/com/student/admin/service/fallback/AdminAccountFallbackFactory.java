package com.student.admin.service.fallback;

import com.student.accountapi.utils.DataResult;
import com.student.admin.service.AdminAccountServiceClient;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class AdminAccountFallbackFactory implements FallbackFactory<AdminAccountServiceClient> {
    @Override
    public AdminAccountServiceClient create(Throwable cause) {
        return new AdminAccountServiceClient() {
            @Override
            public DataResult addUser(Long uid, String uname, String password) {
                return null;
            }

            @Override
            public DataResult delUser(Long uid) {
                return null;
            }

            @Override
            public DataResult find(Long uid, String uname) {
                return null;
            }

            @Override
            public DataResult findAll() {
                return null;
            }

            @Override
            public DataResult update(Long uid, String uname, String password, Integer role, Integer ustatus) {
                return null;
            }
        };
    }
}
