package com.student.admin.service.fallback;

import com.student.admin.service.AdminStuinfoServiceFeign;
import com.student.stuinfo.model.Stuinfo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AdminStuinfoFallback implements AdminStuinfoServiceFeign {
    @Override
    public String deleteStuinfo(String xh) {
        return null;
    }

    @Override
    public Stuinfo findByXh(String xh) {
        return null;
    }

    @Override
    public List<Stuinfo> findAll() {
        return null;
    }

    @Override
    public String addStuinfo(String xh, String sname, Integer sex, Integer age, Integer gid, Integer sstatus) {
        return null;
    }

    @Override
    public String upadateStuinfo(String xh, String sname, Integer sex, Integer age, Integer gid, Integer sstatus) {
        return null;
    }
}
