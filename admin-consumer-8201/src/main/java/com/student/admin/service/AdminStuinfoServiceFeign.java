package com.student.admin.service;


import com.student.admin.service.fallback.AdminStuinfoFallback;
import com.student.stuinfo.model.Stuinfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "stuinfo",fallback = AdminStuinfoFallback.class)
public interface AdminStuinfoServiceFeign {
    @GetMapping("/stuinfo/deleteStuinfo")
    public String deleteStuinfo(@RequestParam("xh")String xh);
    @GetMapping("/stuinfo/findByXh")
    public Stuinfo findByXh(@RequestParam("xh")String xh);
    @GetMapping("/stuinfo/findAllStuinfo")
    public List<Stuinfo> findAll();
    @GetMapping("/stuinfo/addStuinfo")
    public String addStuinfo(@RequestParam("xh")String xh,@RequestParam(value ="sname",required = false)String sname,
                             @RequestParam(value ="sex",required = false)Integer sex,
                             @RequestParam(value ="age",required = false)Integer age,
                             @RequestParam(value ="gid",required = false)Integer gid,
                             @RequestParam(value ="sstatus",required = false)Integer sstatus);
    @GetMapping("/stuinfo/updateStuinfo")
    public String upadateStuinfo(@RequestParam(value = "xh")String xh,
                                 @RequestParam(value ="sname",required = false)String sname,
                                 @RequestParam(value ="sex",required = false)Integer sex,
                                 @RequestParam(value ="age",required = false)Integer age,
                                 @RequestParam(value ="gid",required = false)Integer gid,
                                 @RequestParam(value ="sstatus",required = false)Integer sstatus);
}
