package com.student.admin.service;

import com.student.admin.service.fallback.AdminXClassFallbackFactory;
import com.student.xclassapi.api.AdminServiceFeign;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(
        value = "xclass",
        fallbackFactory = AdminXClassFallbackFactory.class
)
public interface AdminXClassServiceClient extends AdminServiceFeign {


}
