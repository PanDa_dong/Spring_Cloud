package com.student.admin.service;

import com.student.accountapi.api.AdminServiceFeign;
import com.student.admin.service.fallback.AdminXClassFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(
        value = "account",
        fallbackFactory = AdminXClassFallbackFactory.class
)
public interface AdminAccountServiceClient extends AdminServiceFeign {
}
