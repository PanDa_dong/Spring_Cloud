package com.student.admin.service.fallback;

import com.student.admin.service.AdminXClassServiceClient;
import com.student.xclassapi.utils.DataResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class AdminXClassFallbackFactory implements FallbackFactory<AdminXClassServiceClient> {
    @Override
    public AdminXClassServiceClient create(Throwable cause) {
        return new AdminXClassServiceClient() {
            @Override
            public DataResult findAllClass() {
                return new DataResult(500,"请求有误！ msg:"+cause);
            }

            @Override
            public DataResult findClass(Long cid, String cname) {
                return new DataResult(500,"请求有误！ msg:"+cause);
            }

            @Override
            public DataResult addClass(String cname, String info, int cstatus) {
                return new DataResult(500,"请求有误！ msg:"+cause);
            }

            @Override
            public DataResult delClass(Long cid) {
                return new DataResult(500,"请求有误！ msg:"+cause);
            }

            @Override
            public DataResult updateClass(Long cid, String cname, String info, int cstatus) {
                return new DataResult(500,"请求有误！ msg:"+cause);
            }
        };
    }
}
