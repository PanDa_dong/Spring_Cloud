package com.student.admin.service.fallback;

import com.student.admin.service.AdminGradeServiceFeign;
import com.student.xclassapi.utils.DataResult;
import org.springframework.stereotype.Component;

@Component
public class AdminGradeFallback implements AdminGradeServiceFeign {
    @Override
    public DataResult addGrade(Long gid, String gname, Integer gstatus) {
        return new DataResult(500,"请求有误！");
    }

    @Override
    public DataResult delGrade(Long gid) {
        return new DataResult(500,"请求有误！");
    }

    @Override
    public DataResult findAll() {
        return new DataResult(500,"请求有误！");
    }

    @Override
    public DataResult update(Long gid, String gname, Integer gstatus) {
        return new DataResult(500,"请求有误！");
    }
}
