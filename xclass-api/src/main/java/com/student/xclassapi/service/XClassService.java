package com.student.xclassapi.service;

import com.student.xclassapi.api.XClassServiceAPI;
import com.student.xclassapi.constant.XClassServiceURL;
import com.student.xclassapi.utils.DataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;




@Service
@ConditionalOnBean(RestTemplate.class)
public class XClassService implements XClassServiceAPI {
    @Autowired
    private RestTemplate restTemplate;

//    private RestTemplate restTemplates = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

    @Override
    public DataResult findAllClass() {
        return restTemplate.getForObject(XClassServiceURL.PREFIX+XClassServiceURL.ADMIN_FIND_ALL_CLASS, DataResult.class);
    }

    @Override
    public DataResult findClass(Long cid, String cname) {
        return restTemplate.getForObject(XClassServiceURL.PREFIX+XClassServiceURL.ADMIN_FIND_CLASS+XClassServiceURL.ADMIN_FIND_CLASS_PARAMETER,DataResult.class,cid,cname);
    }

    @Override
    public DataResult addClass(String cname, String info, int cstatus) {
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("cname", cname);
        map.add("info", info);
        map.add("cstatus", String.valueOf(cstatus));
        return restTemplate.postForObject(XClassServiceURL.PREFIX+XClassServiceURL.ADMIN_ADD_CLASS,map,DataResult.class);//+XClassServiceURL.ADMIN_ADD_CLASS_PARAMETER
    }

    @Override
    public DataResult delClass(Long cid) {
        return restTemplate.getForObject(XClassServiceURL.PREFIX+XClassServiceURL.ADMIN_DELETE_CLASS+XClassServiceURL.ADMIN_DELETE_CLASS_PARAMETER,DataResult.class,cid);
    }

    @Override
    public DataResult updateClass(Long cid, String cname, String info, int cstatus) {
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("cid", String.valueOf(cid));
        map.add("cname", cname);
        map.add("info", info);
        map.add("cstatus", String.valueOf(cstatus));
        return restTemplate.postForObject(XClassServiceURL.PREFIX+XClassServiceURL.ADMIN_UPDATE_CLASS,map,DataResult.class);
    }

    @Override
    public DataResult sFindAllClass() {
        return restTemplate.getForObject(XClassServiceURL.PREFIX+XClassServiceURL.STUDENT_FIND_ALL_CLASS,DataResult.class);
    }

    @Override
    public DataResult sFindClass(Long cid, String cname) {
        return restTemplate.getForObject(XClassServiceURL.PREFIX+XClassServiceURL.STUDENT_FIND_CLASS+XClassServiceURL.STUDENT_FIND_CLASS_PARAMETER,DataResult.class,cid,cname);
    }
}
