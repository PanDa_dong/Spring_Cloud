package com.student.xclassapi.api;

import com.student.xclassapi.constant.XClassServiceURL;
import com.student.xclassapi.utils.DataResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface StudentServiceFeign {
    @GetMapping(XClassServiceURL.CONTROLLER_MAPPING+XClassServiceURL.STUDENT_FIND_ALL_CLASS)
    public DataResult sFindAll();
    @GetMapping(XClassServiceURL.CONTROLLER_MAPPING+XClassServiceURL.STUDENT_FIND_CLASS)
    public DataResult sFindClass(@RequestParam(value = "cid",required = false) Long cid, @RequestParam(value = "cname",required = false) String cname);
}
