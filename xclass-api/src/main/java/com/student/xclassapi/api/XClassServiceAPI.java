package com.student.xclassapi.api;

import com.student.xclassapi.utils.DataResult;

public interface XClassServiceAPI {
    public DataResult findAllClass();
    public DataResult findClass(Long cid,String cname);
    public DataResult addClass(String cname,String info,int cstatus);
    public DataResult delClass(Long cid);
    public DataResult updateClass(Long cid,String cname,String info,int cstatus);

    public DataResult sFindAllClass();
    public DataResult sFindClass(Long cid,String cname);
}
