package com.student.xclassapi.api;


import com.student.xclassapi.constant.XClassServiceURL;
import com.student.xclassapi.utils.DataResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


public interface AdminServiceFeign {
    @GetMapping(XClassServiceURL.CONTROLLER_MAPPING+XClassServiceURL.ADMIN_FIND_ALL_CLASS)
    public DataResult findAllClass();
    @GetMapping(XClassServiceURL.CONTROLLER_MAPPING+XClassServiceURL.ADMIN_FIND_CLASS)
    public DataResult findClass(@RequestParam(value = "cid",required = false) Long cid, @RequestParam(value = "cname",required = false) String cname);
    @PostMapping(XClassServiceURL.CONTROLLER_MAPPING+XClassServiceURL.ADMIN_ADD_CLASS)
    public DataResult addClass(@RequestParam(value = "cname",required = false) String cname, @RequestParam(value = "info",required = false) String info, @RequestParam(value = "cstatus",required = false) int cstatus);
    @GetMapping(XClassServiceURL.CONTROLLER_MAPPING+XClassServiceURL.ADMIN_DELETE_CLASS)
    public DataResult delClass(@RequestParam(value = "cid",required = false) Long cid);
    @PostMapping(XClassServiceURL.CONTROLLER_MAPPING+XClassServiceURL.ADMIN_UPDATE_CLASS)
    public DataResult updateClass(@RequestParam(value = "cid",required = false) Long cid, @RequestParam(value = "cname",required = false) String cname, @RequestParam(value = "info",required = false) String info, @RequestParam(value = "cstatus",required = false) int cstatus);
}
