package com.student.xclassapi.constant;

public interface XClassServiceURL {
    String PREFIX = "http://xclass/class";
    String CONTROLLER_MAPPING = "/class";
    /*管理员*/
    String ADMIN_FIND_ALL_CLASS = "/admin/findAll";
    String ADMIN_FIND_CLASS = "/admin/find";
    String ADMIN_ADD_CLASS = "/admin/add";
    String ADMIN_DELETE_CLASS = "/admin/del";
    String ADMIN_UPDATE_CLASS = "/admin/update";
    /*管理员参数*/
    String ADMIN_FIND_CLASS_PARAMETER = "?cid={cid}&cname={cname}";
    String ADMIN_DELETE_CLASS_PARAMETER = "?cid={cid}";
    /*学生*/
    String STUDENT_FIND_ALL_CLASS = "/student/findAll";
    String STUDENT_FIND_CLASS = "/student/find";
    /*学生参数*/
    String STUDENT_FIND_CLASS_PARAMETER = "?cid={cid}&cname={cname}";
}
