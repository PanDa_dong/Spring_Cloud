package com.student.xclassapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class XClass {
    private Long cid;
    private String cname;
    private String info;
    private Integer cstatus;
}
