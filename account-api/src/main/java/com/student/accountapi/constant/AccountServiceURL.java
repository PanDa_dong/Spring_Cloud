package com.student.accountapi.constant;

public interface AccountServiceURL {
    //String PREFIX = "http://account/user";
    String CONTROLLER_MAPPING = "/user";
    /*管理员*/
    String ADMIN_FIND_ALL_USER = "/admin/findAll";
    String ADMIN_FIND_USER = "/admin/find";
    String ADMIN_ADD_USER = "/admin/add";
    String ADMIN_DELETE_USER = "/admin/del";
    String ADMIN_UPDATE_USER = "/admin/update";
    /*管理员参数--Feign不需要*/
    //String ADMIN_FIND_USER_PARAMETER = "?uid={uid}&uname={uname}";
    //String ADMIN_DELETE_USER_PARAMETER = "?uid={uid}";
    /*学生*/
    String STUDENT_CHANGE = "/student/change";
    /*学生参数--Feign不需要*/
    //String STUDENT_FIND_USER_PARAMETER = "?uid={uid}&uname={uname}";
}
