package com.student.accountapi.api;

import com.student.accountapi.constant.AccountServiceURL;
import com.student.accountapi.utils.DataResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface StudentServiceFeign {
    @GetMapping(AccountServiceURL.CONTROLLER_MAPPING+AccountServiceURL.STUDENT_CHANGE)
    public DataResult change(@RequestParam(value = "uid",required = false)Long uid, @RequestParam(value = "oldPWD",required = false)String oldPWD, @RequestParam(value = "newPWD",required = false)String newPWD);
}
