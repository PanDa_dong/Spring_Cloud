package com.student.accountapi.api;

import com.student.accountapi.constant.AccountServiceURL;
import com.student.accountapi.utils.DataResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface AdminServiceFeign {
    @PostMapping(AccountServiceURL.CONTROLLER_MAPPING+AccountServiceURL.ADMIN_ADD_USER)
    public DataResult addUser(@RequestParam(value = "uid",required = false)Long uid, @RequestParam(value = "uname",required = false)String uname, @RequestParam(value = "password",required = false) String password);
    @GetMapping(AccountServiceURL.CONTROLLER_MAPPING+AccountServiceURL.ADMIN_DELETE_USER)
    public DataResult delUser(@RequestParam(value = "uid",required = false)Long uid);
    @GetMapping(AccountServiceURL.CONTROLLER_MAPPING+AccountServiceURL.ADMIN_FIND_USER)
    public DataResult find(@RequestParam(value = "uid",required = false)Long uid,@RequestParam(value = "uname",required = false)String uname);
    @GetMapping(AccountServiceURL.CONTROLLER_MAPPING+AccountServiceURL.ADMIN_FIND_ALL_USER)
    public DataResult findAll();
    @PostMapping(AccountServiceURL.CONTROLLER_MAPPING+AccountServiceURL.ADMIN_UPDATE_USER)
    public DataResult update(@RequestParam(value = "uid",required = false)Long uid, @RequestParam(value = "uname",required = false)String uname, @RequestParam(value = "password",required = false) String password,@RequestParam(value = "role",required = false)Integer role,@RequestParam(value = "ustatus",required = false)Integer ustatus);

}
