package com.student.accountapi.api;

import com.student.accountapi.utils.DataResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface AccountServiceAPI {
    @RequestMapping
    public DataResult findAllUser();
    public DataResult findUser(Long uid,String uname);
    public DataResult addUser(Long uid, String uname,  String password);
    public DataResult delUser(Long uid);
    public DataResult updateUser(Long uid,String uname,String info,int cstatus);


    public DataResult sFindUser(Long uid,String uname);
}
