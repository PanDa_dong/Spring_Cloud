package com.student.accountapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Long uid;
    private String uname;
    private String password;
    private Integer role;//0-管理员1-学生
    private Integer ustatus;//0-不可用1-可用
}
