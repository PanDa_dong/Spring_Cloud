package com.student.gateway.filter;


import com.student.gateway.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.List;


@Slf4j
@Component
public class RequestLogFilter implements GlobalFilter, Ordered {
    @Autowired
    private RedisUtil redisUtil;


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String uri = request.getURI().toString();

        //登录--放行
        if (uri.contains("login")) {
            return chain.filter(exchange);
        }


        //判断是否登录过
        HttpHeaders test = exchange.getRequest().getHeaders();
        System.out.println("uri:" + uri);//url:/user/count
        //String uri = exchange.getRequest().getURI().toString();
        //System.out.println("uri:"+uri);//uri:http://localhost:8000/user/count
        String token = null;
        List<String> authorizations = test.get("Authorization");
        if (authorizations.size() != 0) {
            for (String Authname : authorizations) {
                log.info("Authname:" + Authname);
                Authname = Authname.replace("Bearer ", "");
                if (redisUtil.hasKey(Authname)) {
                    //查询redis是否有token
                    token = redisUtil.get(Authname).toString();
                    log.info("token:" + token);
                    return chain.filter(exchange);
                }
            }
        }
        //没有token不放行
        if (token == null) {
            log.info("********token为null******** ");
            response.setStatusCode(HttpStatus.FORBIDDEN);
            return response.setComplete();
        }else {

        }

        //没有token不放行
        response.setStatusCode(HttpStatus.FORBIDDEN);
        return response.setComplete();



/*        if(StringUtils.isBlank(Authname)){
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            ctx.setResponseBody("{\"code\":401,\"msg\":\"没有访问权限！\"}");
            ctx.getResponse().setContentType("text/html;charset=UTF-8");
        }else if(token==null){
            //token失效了
            //用户提供的token检测出和redis不一样
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            ctx.setResponseBody("{\"code\":401,\"msg\":\"令牌失效,请重新登录！\"}");
            ctx.getResponse().setContentType("text/html;charset=UTF-8");
        }*/










        /*log.info("****************** come in my log gateway filter"+new Date());
        String uname = exchange.getRequest().getQueryParams().getFirst("uname");

        if (uname == null){
            log.info("****************用户名为null，非法用户 ");
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            return exchange.getResponse().setComplete();
        }

//=======================================================================
        Consumer<HttpHeaders> httpHeaders = httpHeader -> {
            httpHeader.set("aaaa", "bbb");
            httpHeader.set("xxx", "cc");
            httpHeader.set("bbxx", "bbbx");
            httpHeader.set("aaaa", "bbb");
        };
        ServerHttpRequest serverHttpRequest = exchange.getRequest().mutate().headers(httpHeaders).build();
        exchange.mutate().request(serverHttpRequest).build();

//=======================================================================

        String token = exchange.getRequest().getQueryParams().getFirst("authToken");
        //向headers中放文件，记得build
        ServerHttpRequest host = exchange.getRequest().mutate().header("a", "888").build();
        //将现在的request 变成 change对象
        ServerWebExchange build = exchange.mutate().request(host).build();
//        return chain.filter(build);

*/
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
