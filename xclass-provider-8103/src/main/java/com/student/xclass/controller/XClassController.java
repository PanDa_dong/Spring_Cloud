package com.student.xclass.controller;

import com.student.xclass.entity.XClass;
import com.student.xclass.service.XClassService;
import com.student.xclassapi.api.AdminServiceFeign;
import com.student.xclassapi.api.StudentServiceFeign;
import com.student.xclassapi.constant.XClassServiceURL;
import com.student.xclassapi.utils.DataResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping(XClassServiceURL.CONTROLLER_MAPPING)
public class XClassController implements AdminServiceFeign , StudentServiceFeign {
    @Resource
    XClassService xClassService;


    /*===============================管理员===============================*/
    @GetMapping(XClassServiceURL.ADMIN_FIND_ALL_CLASS)
    public DataResult findAllClass() {
        List<XClass> all = xClassService.findAll();
        return new DataResult(200, "所有课程", all);
    }

    @GetMapping(XClassServiceURL.ADMIN_FIND_CLASS)
    public DataResult findClass(@RequestParam("cid") Long cid, @RequestParam("cname") String cname) {
        List<XClass> all = xClassService.findClass(cid, cname);
        return new DataResult(200, cname, all);
    }

    @PostMapping(XClassServiceURL.ADMIN_ADD_CLASS)
    public DataResult addClass(@RequestParam("cname") String cname, @RequestParam("info") String info, @RequestParam("cstatus") int cstatus) {
        XClass xclass = new XClass();
        xclass.setCname(cname);
        xclass.setInfo(info);
        xclass.setCstatus(cstatus);
        int result = xClassService.addXClass(xclass);
        if (result != 0) {
            return new DataResult(200, "新增课程成功！", null);
        } else {
            return new DataResult(400, "操作失败，请重试！", null);
        }
    }

    @GetMapping(XClassServiceURL.ADMIN_DELETE_CLASS)
    public DataResult delClass(@RequestParam("cid") Long cid) {
        int result = xClassService.deleteXClassByCid(cid);
        if (result != 0) {
            return new DataResult(200, "删除课程成功！", null);
        } else {
            return new DataResult(400, "操作失败，请重试！", null);
        }
    }

    @PostMapping(XClassServiceURL.ADMIN_UPDATE_CLASS)
    public DataResult updateClass(@RequestParam("cid") Long cid, @RequestParam("cname") String cname, @RequestParam("info") String info, @RequestParam("cstatus") int cstatus) {
        XClass xclass = new XClass();
        xclass.setCid(cid);
        xclass.setCname(cname);
        xclass.setInfo(info);
        xclass.setCstatus(cstatus);
        int result = xClassService.updateXClassByCid(xclass);
        if (result != 0) {
            return new DataResult(200, "更新课程成功！", null);
        } else {
            return new DataResult(400, "操作失败，请重试！", null);
        }
    }
    /*===============================学生===============================*/



    @GetMapping(XClassServiceURL.STUDENT_FIND_ALL_CLASS)
    public DataResult sFindAll() {
        List<XClass> all = xClassService.sFindAll();
        return new DataResult(200, "所有课程", all);
    }

    @GetMapping(XClassServiceURL.STUDENT_FIND_CLASS)
    public DataResult sFindClass(@RequestParam("cid") Long cid, @RequestParam("cname") String cname) {
        List<XClass> all = xClassService.sFindClass(cid, cname);
        return new DataResult(200, cname, all);
    }
}
