package com.student.xclass;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class XClassApplication {
    public static void main(String[] args) {
        SpringApplication.run(XClassApplication.class, args);
    }

}
