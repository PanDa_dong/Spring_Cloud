package com.student.xclass.service.impl;

import com.student.xclass.dao.XClassDao;
import com.student.xclass.entity.XClass;
import com.student.xclass.service.XClassService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class XClassServiceImpl implements XClassService {
    @Resource
    private XClassDao xClassDao;

    /*管理员*/
    @Override
    public List<XClass> findAll() {
        return xClassDao.findAll();
    }


    @Override
    public List<XClass> findClass(Long cid, String cname) {
        return xClassDao.findClass(cid, cname);
    }

    @Override
    public Integer addXClass(XClass xClass) {
        return xClassDao.addXClass(xClass);
    }

    @Override
    public Integer deleteXClassByCid(Long cid) {
        return xClassDao.deleteXClass(cid);
    }

    @Override
    public Integer updateXClassByCid(XClass xClass) {
        return xClassDao.updateXClass(xClass);
    }

    /*学生*/

    @Override
    public List<XClass> sFindAll() {
        return xClassDao.sFindAll();
    }

    @Override
    public List<XClass> sFindClass(Long cid, String cname) {
        return xClassDao.sFindClass(cid,cname);
    }
}
