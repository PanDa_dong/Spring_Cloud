package com.student.xclass.service;

import com.student.xclass.entity.XClass;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface XClassService {
    /*管理员*/
    public List<XClass> findAll();

    public List<XClass> findClass(Long cid, String cname);

    public Integer addXClass(XClass xClass);

    public Integer deleteXClassByCid(Long cid);

    public Integer updateXClassByCid(XClass xClass);

    /*学生*/
    public List<XClass> sFindAll();

    public List<XClass> sFindClass(Long cid, String cname);
}
