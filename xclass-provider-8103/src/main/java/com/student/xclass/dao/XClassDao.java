package com.student.xclass.dao;


import com.student.xclass.entity.XClass;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface XClassDao {
    //管理员
    public List<XClass> findAll();

    public List<XClass> findClass(@Param("cid") Long cid, @Param("cname") String cname);

    public Integer addXClass(@Param("class") XClass xClass);

    public Integer deleteXClass(@Param("cid") Long cid);

    public Integer updateXClass(@Param("class") XClass xClass);
    //学生
    public List<XClass> sFindAll();

    public List<XClass> sFindClass(@Param("cid") Long cid, @Param("cname") String cname);



}
