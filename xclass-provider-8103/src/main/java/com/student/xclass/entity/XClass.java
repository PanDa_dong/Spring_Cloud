package com.student.xclass.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class XClass implements Serializable {
    private static final long serialVersionUID = 123L;
    private Long cid;
    private String cname;
    private String info;
    private Integer cstatus;
}
