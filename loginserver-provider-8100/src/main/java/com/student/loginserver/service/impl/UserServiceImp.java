package com.student.loginserver.service.impl;

import com.student.loginserver.dao.UserDao;
import com.student.loginserver.entitys.User;
import com.student.loginserver.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImp implements UserService {

    @Resource
    private UserDao userDao;

    @Override
    public User login(String uname, String password) {
        return userDao.login(uname, password);
    }

    @Override
    public int count() {
        return userDao.count();
    }
}
