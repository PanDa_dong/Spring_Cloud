package com.student.loginserver.service;


import com.student.loginserver.entitys.User;

public interface UserService {
    User login(String uname, String password);

    int count();
}
