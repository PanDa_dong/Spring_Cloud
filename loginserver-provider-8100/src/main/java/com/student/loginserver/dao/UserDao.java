package com.student.loginserver.dao;

import com.student.loginserver.entitys.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {
    public User login(String uname, String password);

    public int count();
}
