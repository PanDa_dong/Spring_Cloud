package com.student.loginserver.controller;

import com.student.loginserver.entitys.User;
import com.student.loginserver.service.UserService;

import com.student.loginserver.utils.DataResult;
import com.student.loginserver.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RestController
@Slf4j
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {
    @Resource
    private UserService userService;

    /*    @Autowired
        @Qualifier("redisTemplate")
        private RedisTemplate redisTemplate;*/
    @Autowired
    private RedisUtil redisUtil;


    //    @Value("${redis.expireTime}")
    private Long expireTime = 86400L;//token过期时间


    @GetMapping(path = "/login")
    @ResponseBody
    public DataResult login(String uname, String password, HttpServletResponse response) throws Exception {

        try {
            System.out.println("uname=" + uname + "  password=" + password);
            User user = userService.login(uname, password);
            if (user != null) {
                String token = TokenStorage(response, user.getRole());
                log.info("登录成功,存储token");
                return new DataResult(200, "登录成功", "Bearer " + token);
            } else {
                log.info("登录失败,用户名或密码错误");
                return new DataResult(444, "用户名或密码错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new DataResult(500, "未知错误");
    }


    @GetMapping(value = "/count")
    public String count() {
        int sum = userService.count();
        return "总条数为" + sum;
    }

    public String TokenStorage(HttpServletResponse response, int role) {
        //生成token
        String token = UUID.randomUUID().toString().replaceAll("-", "");
        //存储redis里
        redisUtil.set(token, role, expireTime);//string time obj
        return token;
    }

}
