package com.student.student_consumer.service;

import com.student.accountapi.api.StudentServiceFeign;
import com.student.student_consumer.service.fallback.StudentAccountFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(
        value = "account",
        //fallback = AccountServiceFeitnERROR.class,
        fallbackFactory = StudentAccountFallbackFactory.class
)
public interface StudentAccountServiceClient extends StudentServiceFeign {

}
//StudentAccountServiceClient#change(Long,String,String)