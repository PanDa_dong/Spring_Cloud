package com.student.student_consumer.service;

import com.student.student_consumer.service.fallback.StudentXClassFallbackFactory;
import com.student.xclassapi.api.StudentServiceFeign;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(
        value = "xclass",
        fallbackFactory = StudentXClassFallbackFactory.class
)
public interface StudentXClassServiceClient extends StudentServiceFeign {
}
