package com.student.student_consumer.service.fallback;

import com.student.student_consumer.service.StudentXClassServiceClient;
import com.student.xclassapi.utils.DataResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class StudentXClassFallbackFactory implements FallbackFactory<StudentXClassServiceClient> {
    @Override
    public StudentXClassServiceClient create(Throwable cause) {
        return new StudentXClassServiceClient() {
            @Override
            public DataResult sFindAll() {
                return new DataResult(500,"请求有误！ msg:"+cause);
            }

            @Override
            public DataResult sFindClass(Long cid, String cname) {
                return new DataResult(500,"请求有误！ msg:"+cause);
            }
        };
    }
}
