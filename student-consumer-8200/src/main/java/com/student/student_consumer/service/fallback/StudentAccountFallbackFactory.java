package com.student.student_consumer.service.fallback;

import com.student.accountapi.utils.DataResult;
import com.student.student_consumer.service.StudentAccountServiceClient;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class StudentAccountFallbackFactory implements FallbackFactory<StudentAccountServiceClient> {
    @Override
    public StudentAccountServiceClient create(Throwable cause) {
        return new StudentAccountServiceClient() {
            @Override
            public DataResult change(Long uid, String oldPWD, String newPWD) {
                return new DataResult(500,"请求有误！ msg:"+cause);
            }
        };
    }
}
