package com.student.student_consumer.service;

import com.student.xclassapi.api.XClassServiceAPI;
import com.student.xclassapi.utils.DataResult;
import org.springframework.stereotype.Service;

//@Service
public class StudentService implements XClassServiceAPI {
    @Override
    public DataResult findAllClass() {
        return null;
    }

    @Override
    public DataResult findClass(Long cid, String cname) {
        return null;
    }

    @Override
    public DataResult addClass(String cname, String info, int cstatus) {
        return null;
    }

    @Override
    public DataResult delClass(Long cid) {
        return null;
    }

    @Override
    public DataResult updateClass(Long cid, String cname, String info, int cstatus) {
        return null;
    }

    @Override
    public DataResult sFindAllClass() {
        return null;
    }

    @Override
    public DataResult sFindClass(Long cid, String cname) {
        return null;
    }
}
