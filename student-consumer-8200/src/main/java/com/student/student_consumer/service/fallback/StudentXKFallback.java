package com.student.student_consumer.service.fallback;

import com.student.student_consumer.service.StudentXKServiceFeign;
import com.student.xclassapi.entity.XClass;
import com.student.xclassapi.utils.DataResult;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StudentXKFallback implements StudentXKServiceFeign {

    @Override
    public DataResult addXK(Long xh, Long cid) {
        return new DataResult(500,"请求有误！");
    }

    @Override
    public DataResult find(Long xh) {
        return new DataResult(500,"请求有误！");
    }
}
