package com.student.student_consumer.service;

import com.student.student_consumer.service.fallback.StudentXKFallback;
import com.student.xclassapi.entity.XClass;
import com.student.xclassapi.utils.DataResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "xk",fallback = StudentXKFallback.class)
public interface StudentXKServiceFeign {
    @GetMapping("/xk/add")
    public DataResult addXK(@RequestParam("xh")Long xh,@RequestParam("cid")Long cid);
    @GetMapping("/xk/find")
    public DataResult find(@RequestParam("xh")Long xh);
}
