package com.student.student_consumer.controller;

//import com.student.xclassapi.service.XClassService;


import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.student.student_consumer.service.StudentAccountServiceClient;
import com.student.student_consumer.service.StudentXClassServiceClient;
import com.student.student_consumer.util.RedisUtil;
import com.student.xclassapi.utils.DataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/student/class")
@DefaultProperties(defaultFallback = "UNKnowFile")
public class StudentXClassController {
    //api调用方式
    /*@Qualifier("XClassService")
    @Autowired
    private XClassServiceAPI xClassService;*/
    //feign调用服务提供者
    @Autowired
    private StudentXClassServiceClient xClassService;



    /*redis工具类*/
    private RedisUtil redisUtil = new RedisUtil();

    /*查询所有课程*/
    @GetMapping("/findAll")
    /*@HystrixCommand(
            threadPoolKey = "student-findAll",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize",value = "10"),
                    @HystrixProperty(name = "maxQueueSize",value = "20")
            },
            commandProperties = {
                    *//*
                    * 请求超时时间为8秒
                    * 在3秒的时间窗内超过20个请求其中有70%请求出错则开启断路器
                    * 开启短路器后每3秒一个活动窗的时间内放行1个请求如果成功返回则关闭短路器
                    * *//*
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "8000"),//超时8秒
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds",value = "3000"),//时间窗口3秒
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "20"),//最小请求数20个
                    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "70"),//阈值为70%
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "3000")//活动窗口3秒
            }
    )*/
    public DataResult sFindAll() {
        return xClassService.sFindAll();
    }

    /*查询课程*/
    @GetMapping("/find")
/*    @HystrixCommand(
            threadPoolKey = "student-find",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize",value = "3"),
                    @HystrixProperty(name = "maxQueueSize",value = "15")
            },
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "5000"),//超时5秒
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds",value = "3000"),//时间窗口3秒
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "20"),//最小请求数20个
                    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "60"),//阈值为60%
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "3000")//活动窗口3秒
            }
    )*/
    public DataResult sFindClass(@RequestParam("cid") Long cid, @RequestParam("cname") String cname){
        return xClassService.sFindClass(cid,cname);
    }


    /* 备用方案*/
    public DataResult UNKnowFile(){
        return new DataResult(500,"内部服务器出错！");
    }

    /*身份验证*/
    public boolean verification(String uuid){
        Object role = redisUtil.get(uuid);
        if (role==null)return false;
        if (role.toString().equals("1")){
            return true;
        }else{
            return false;
        }
    }
}
