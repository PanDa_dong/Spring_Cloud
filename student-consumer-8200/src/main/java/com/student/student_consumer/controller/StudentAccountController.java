package com.student.student_consumer.controller;

import com.student.accountapi.utils.DataResult;
import com.student.student_consumer.service.StudentAccountServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student/account")
public class StudentAccountController {
    @Autowired
    private StudentAccountServiceClient AccountService;
    /*修改密码*/
    @GetMapping("/change")
    public DataResult change(Long uid, String oldPWD, String newPWD){
        return AccountService.change(uid, oldPWD, newPWD);
    }
}
