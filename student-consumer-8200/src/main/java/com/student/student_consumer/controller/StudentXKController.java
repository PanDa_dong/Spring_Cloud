package com.student.student_consumer.controller;

import com.student.student_consumer.service.StudentXKServiceFeign;
import com.student.xclassapi.entity.XClass;
import com.student.xclassapi.utils.DataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/student/xk")
public class StudentXKController {
    @Autowired
    private StudentXKServiceFeign xkServer;

    /*选课*/
    @GetMapping("/add")
    public DataResult add(@RequestParam("xh")Long xh, @RequestParam("cid")Long cid){
        return xkServer.addXK(xh, cid);
    }

    @GetMapping("/find")
    public DataResult find(@RequestParam("xh")Long xh){
        return xkServer.find(xh);
    }


}
