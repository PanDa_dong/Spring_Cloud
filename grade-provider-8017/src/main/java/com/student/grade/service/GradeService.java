package com.student.grade.service;

import com.student.grade.entity.Grade;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GradeService {
    public Integer addGrade(@Param("grade") Grade grade);

    public Integer delGrade(@Param("gid")Long gid);

    public List<Grade> findAll();

    public Integer updateGrade(@Param("grade") Grade grade);
}
