package com.student.grade.service.impl;

import com.student.grade.dao.GradeDao;
import com.student.grade.entity.Grade;
import com.student.grade.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class GradeServiceImpl implements GradeService {
    @Resource
    private GradeDao gradeDao;
    @Override
    public Integer addGrade(Grade grade) {
        return gradeDao.addGrade(grade);
    }

    @Override
    public Integer delGrade(Long gid) {
        return gradeDao.delGrade(gid);
    }

    @Override
    public List<Grade> findAll() {
        return gradeDao.findAll();
    }

    @Override
    public Integer updateGrade(Grade grade) {
        return gradeDao.updateGrade(grade);
    }
}
