package com.student.grade.controller;

import com.student.grade.entity.Grade;
import com.student.grade.service.GradeService;
import com.student.grade.utils.DataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/grade")
public class GradeController {
    @Autowired
    private GradeService gradeService;
    @PostMapping("/add")
    public DataResult addGrade(@RequestParam("gid")Long gid, @RequestParam("gname")String gname, @RequestParam("gstatus") Integer gstatus){
        Grade grade = new Grade();
        grade.setGid(gid);
        grade.setGname(gname);
        grade.setGstatus(gstatus);
        int result = gradeService.addGrade(grade);
        if (result != 0) {
            return new DataResult(200, "新增年级成功！", null);
        } else {
            return new DataResult(400, "操作失败，请重试！", null);
        }
    }
    @GetMapping("/del")
    public DataResult delGrade(@RequestParam("gid")Long gid){
        int result =gradeService.delGrade(gid);
        if (result != 0) {
            return new DataResult(200, "删除年级成功！", null);
        } else {
            return new DataResult(400, "操作失败年级不存在，请重试！", null);
        }
    }
    @GetMapping("/findAll")
    public DataResult findAll(){
        List<Grade> result=gradeService.findAll();
        return new DataResult(200,"查询所有年级",result);
    }
    @PostMapping("/update")
    public DataResult update(@RequestParam("gid")Long gid, @RequestParam("gname")String gname, @RequestParam("gstatus") Integer gstatus){
        Grade grade = new Grade();
        grade.setGid(gid);
        grade.setGname(gname);
        grade.setGstatus(gstatus);
        int result = gradeService.updateGrade(grade);
        if (result != 0) {
            return new DataResult(200, "更新用户成功！", null);
        } else {
            return new DataResult(400, "操作失败，请重试！", null);
        }
    }
}
