package com.student.account.entity;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 001L;
    private Long uid;
    private String uname;
    private String password;
    private Integer role;//0-管理员1-学生
    private Integer ustatus;//0-不可用1-可用

}
