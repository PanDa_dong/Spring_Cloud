package com.student.account.service;

import com.student.account.entity.User;

import java.util.List;

public interface AccountService {
    /*管理员*/
    public Integer addUser(User user);

    public Integer delUser(Long uid);

    public List<User> find(Long uid, String uname);

    public List<User> findAll();

    public Integer updateUser(User user);

    public Integer updatePassword(Long uid,String oldPWD,String newPWD);

    /*学生*/
}
