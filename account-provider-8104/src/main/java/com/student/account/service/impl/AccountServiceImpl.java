package com.student.account.service.impl;

import com.student.account.dao.AccountDao;
import com.student.account.entity.User;
import com.student.account.service.AccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    @Resource
    private AccountDao accountDao;

    @Override
    public Integer addUser(User user) {
        return accountDao.addUser(user);
    }

    @Override
    public Integer delUser(Long uid) {
        return accountDao.delUser(uid);
    }

    @Override
    public List<User> find(Long uid, String uname) {
        return accountDao.find(uid,uname);
    }

    @Override
    public List<User> findAll() {
        return accountDao.findAll();
    }

    @Override
    public Integer updateUser(User user) {
        return accountDao.updateUser(user);
    }

    @Override
    public Integer updatePassword(Long uid,String oldPWD,String newPWD) {
        String temp = accountDao.verification(uid,oldPWD);
        System.out.println(temp);
        if (temp==null){
            return 0;
        }
        if (temp.equals(oldPWD)){
            int result=accountDao.updatePasswd(uid,newPWD);
            return result;
        }
        else {
            return 0;
        }
    }
}
