package com.student.account.dao;

import com.student.account.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AccountDao {
    /*管理员*/
    public Integer addUser(@Param("user")User user);

    public Integer delUser(@Param("uid")Long uid);

    public List<User> find(@Param("uid")Long uid, @Param("uname")String uname);

    public List<User> findAll();

    public Integer updateUser(@Param("user")User user);

    /*学生*/
    /*验证旧的密码*/
    public String verification(@Param("uid")Long uid,@Param("old")String old);

    public Integer updatePasswd(@Param("uid")Long uid,@Param("password")String password);



}
