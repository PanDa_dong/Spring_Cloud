package com.student.account.controller;

import com.student.account.entity.User;
import com.student.account.service.AccountService;
import com.student.accountapi.api.AdminServiceFeign;
import com.student.accountapi.api.StudentServiceFeign;
import com.student.accountapi.constant.AccountServiceURL;
import com.student.accountapi.utils.DataResult;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AccountServiceURL.CONTROLLER_MAPPING)
public class AccountController implements AdminServiceFeign, StudentServiceFeign {
    @Autowired
    private AccountService accountService;

    /*================================管理员================================*/
    @PostMapping(AccountServiceURL.ADMIN_ADD_USER)
    public DataResult addUser(@RequestParam("uid")Long uid, @RequestParam("uname")String uname, @RequestParam("password") String password){
        User user = new User();
        user.setUid(uid);
        user.setUname(uname);
        user.setPassword(password);
        int result = accountService.addUser(user);
        if (result != 0) {
            return new DataResult(200, "新增用户成功！", null);
        } else {
            return new DataResult(400, "操作失败，请重试！", null);
        }
    }

    @GetMapping(AccountServiceURL.ADMIN_DELETE_USER)
    public DataResult delUser(@RequestParam("uid")Long uid){
        int result =accountService.delUser(uid);
        if (result != 0) {
            return new DataResult(200, "删除用户成功！", null);
        } else {
            return new DataResult(400, "操作失败用户不存在，请重试！", null);
        }
    }

    @GetMapping(AccountServiceURL.ADMIN_FIND_USER)
    public DataResult find(@RequestParam("uid")Long uid,@RequestParam("uname")String uname){
        List<User> result = accountService.find(uid,uname);
        return new DataResult(200,uid+":"+uname,result);
    }

    @GetMapping(AccountServiceURL.ADMIN_FIND_ALL_USER)
    public DataResult findAll(){
        List<User> result=accountService.findAll();
        return new DataResult(200,"查询所有用户",result);
    }

    @PostMapping(AccountServiceURL.ADMIN_UPDATE_USER)
    public DataResult update(@RequestParam("uid")Long uid, @RequestParam("uname")String uname, @RequestParam("password") String password,@RequestParam("role")Integer role,@RequestParam("ustatus")Integer ustatus){
        User user = new User();
        user.setUid(uid);
        user.setUname(uname);
        user.setPassword(password);
        user.setRole(role);
        user.setUstatus(ustatus);
        int result = accountService.updateUser(user);
        if (result != 0) {
            return new DataResult(200, "更新用户成功！", null);
        } else {
            return new DataResult(400, "操作失败，请重试！", null);
        }
    }

    /*================================学生================================*/
    @GetMapping(AccountServiceURL.STUDENT_CHANGE)
    public DataResult change(@RequestParam("uid")Long uid, @RequestParam("oldPWD")String oldPWD, @RequestParam("newPWD")String newPWD){
        int result = accountService.updatePassword(uid,oldPWD,newPWD);
        if (result != 0) {
            return new DataResult(200, "更改密码成功！", null);
        } else {
            return new DataResult(400, "密码不正确！", null);
        }
    }

}
